﻿using System;
using MyClassLib.WorldOfTanks;

namespace Day7__Tanks_
{
    class Program
    {
        static void Main(string[] args)
        {
            int size = 5;
            int numberOfWinsT34 = 0;
            int numberOfWinsPanters = 0;
            Tank[] tanksT34 = new Tank[size];
            Tank[] tanksPanthers = new Tank[size];
            for (int i = 0; i < size; i++)
            {
                tanksT34[i] = new Tank("T-34");
                tanksPanthers[i] = new Tank("Pantera");
            }
            for (int i = 0; i < size; i++)
            {
                Console.WriteLine($"Бой №{i + 1}: ");
                TankParameters.ToTankParameters(tanksT34[i]);
                TankParameters.ToTankParameters(tanksPanthers[i]);
                if (tanksT34[i] * tanksPanthers[i] == "T-34")
                {
                    Console.WriteLine("В бою победил танк T-34");
                    numberOfWinsT34++;
                }
                else if (tanksT34[i] * tanksPanthers[i] == "Pantera")
                {
                    Console.WriteLine("В бою победил танк Pantera");
                    numberOfWinsPanters++;
                }
                Console.WriteLine("");
            }
            Console.WriteLine("---------------");
            if (numberOfWinsT34 > numberOfWinsPanters)
            {
                Console.WriteLine($"Со счетом {numberOfWinsT34}:{numberOfWinsPanters} победили танки - T-34");
            }
            else
            {
                Console.WriteLine($"Со счетом {numberOfWinsT34}:{numberOfWinsPanters} победили танки - Pantera");
            }
        }
    }
}
