﻿using System;

namespace MyClassLib.WorldOfTanks
{
    public class Tank
    {
        public string Name { get; set; }
        public int AmmunitionLevel { get; set; }
        public int ArmorLevel { get; set; }
        public int ManeuverabilityLevel { get; set; }
        private Random random = new Random();
        
        public Tank(string name)
        {
            Name=name;
            AmmunitionLevel = random.Next(0, 100);
            ArmorLevel = random.Next(0, 100);
            ManeuverabilityLevel = random.Next(0, 100);
        }

        public static string operator * (Tank firstTank, Tank secondTank)
        {
            if ((firstTank.AmmunitionLevel>=secondTank.AmmunitionLevel&&firstTank.ArmorLevel>=secondTank.ArmorLevel)||
                (firstTank.ArmorLevel>=secondTank.ArmorLevel&&firstTank.ManeuverabilityLevel>=secondTank.ManeuverabilityLevel)||
                (firstTank.AmmunitionLevel >= secondTank.AmmunitionLevel && firstTank.ManeuverabilityLevel >= secondTank.ManeuverabilityLevel))
            {
                return firstTank.Name;
            }
            else
            {
                return secondTank.Name;
            }
        }
    }
}
