﻿using System;

namespace MyClassLib.WorldOfTanks
{
    public class TankParameters
    {
        public static void ToTankParameters(Tank tank)
        {
            Console.WriteLine($"Танк - {tank.Name} (БК:{tank.AmmunitionLevel} - БР:{tank.ArmorLevel} - МАНЕВР:{tank.ManeuverabilityLevel})");
        }
    }
}
